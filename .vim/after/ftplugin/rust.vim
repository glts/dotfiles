setlocal textwidth=80

let b:radical_bases = {8: {'format': '0o%s'}}

if exists('b:undo_ftplugin')
  let b:undo_ftplugin .= ' | setlocal textwidth<'
else
  let b:undo_ftplugin = 'setlocal textwidth<'
endif
let b:undo_ftplugin .= ' | unlet! b:radical_bases'
