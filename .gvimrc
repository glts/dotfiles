set cursorline

set lines=50
set columns=120

" Disable GUI features (toolbars, popups, tabs).
set guioptions-=T
set guioptions-=m
set guioptions+=c
set guioptions-=e

colorscheme molokai
